package com.aadalid.crud.controller;

import com.aadalid.crud.model.Note;
import com.aadalid.crud.service.NoteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@Controller
public class NoteController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final int ROW_PER_PAGE = 1;

    @Autowired
    private NoteService noteService;

    @Value("${msg.title}")
    private String title;

    @GetMapping(value = {"/", "/index"})
    public String index(Model model) {
        model.addAttribute("title", title);
        return "index"; // ****
    }

    @GetMapping("/notes")
    public String getNotes(Model model, @RequestParam(value = "page", defaultValue = "1") int pageNumber) {
        List<Note> notes = noteService.findAll(pageNumber, ROW_PER_PAGE);
        long count = noteService.count();

        boolean hasPrev = pageNumber > 1;
        boolean hasNext = (pageNumber*ROW_PER_PAGE) < count;

        model.addAttribute("notes", notes);
        model.addAttribute("hasPrev", hasPrev);
        model.addAttribute("prev", pageNumber - 1);
        model.addAttribute("hasNext", hasNext);
        model.addAttribute("next", pageNumber + 1);
        return "note-list";
    }

    @GetMapping("/notes/{noteId}")
    public String getNoteById(Model model, @PathVariable long noteId) {
        Note note = null;

        try {
            note = noteService.findById(noteId);
            model.addAttribute("allowDelete", false);
        }catch (Exception e){
            model.addAttribute("errorMessage", e.getMessage());
        }
        model.addAttribute("note", note);
        return "note";
    }

    @GetMapping("/notes/add")
    public String showAddNote(Model model) {
        Note note = new Note();
        model.addAttribute("add", true);
        model.addAttribute("note", note);
        return "note-edit";
    }

    @PostMapping("/notes/add")
    public String addNote(Model model, @ModelAttribute("note") Note note) {
        try {
            Note newNote = noteService.save(note);
            return "redirect:/notes/" + String.valueOf(newNote.getId());
        } catch (Exception e) {
            // log exception
            // show error
            String erroMsg = e.getMessage();
            logger.error(erroMsg);

            model.addAttribute("errorMessage", erroMsg);
            model.addAttribute("add", true);

            return "note-edit";
        }
    }

    @GetMapping("/notes/{noteId}/edit")
    public String showEditNote(Model model, @PathVariable long noteId) {
        Note note = null;

        try {
            note = noteService.findById(noteId);
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.getMessage());
        }

        model.addAttribute("add", false);
        model.addAttribute("note", note);

        return "note-edit";
    }

    @PostMapping("/notes/{noteId}/edit")
    public String updateNote(Model model, @PathVariable long noteId, @ModelAttribute("note") Note note){
        System.out.println("NOTE DETAILES :");
        System.out.println(note.getId());
        System.out.println(note.getTitle());
        System.out.println(note.getContent());
        System.out.println(note.getCreatedOn());
        System.out.println(note.getUpdatedOn());
        try {
            note.setId(noteId);
            noteService.update(note);

            return "redirect:/notes/" + String.valueOf(note.getId());
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            logger.error(errorMessage);
            model.addAttribute("errorMessage", errorMessage);

            model.addAttribute("add", false);
            return "note-edit";
        }
    }

    @GetMapping("/notes/{noteId}/delete")
    public String showDeleteNoteById(Model model, @PathVariable long noteId){
        try{
            Note note = noteService.findById(noteId);
            model.addAttribute("note", note);
            model.addAttribute("allowDelete", true);

        } catch (Exception e) {
            model.addAttribute("errorMessage", e.getMessage());
        }

        return "note";
    }

    @PostMapping("/notes/{noteId}/delete")
    public String deleteNoteById(Model model, @PathVariable long noteId){
        try {
            noteService.deleteById(noteId);
        }catch (Exception e) {
            model.addAttribute("errorMessage", e.getMessage());
        }

        return "redirect:/notes/";
    }

}
