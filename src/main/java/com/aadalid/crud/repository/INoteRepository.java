package com.aadalid.crud.repository;

import com.aadalid.crud.model.Note;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface INoteRepository extends PagingAndSortingRepository<Note, Long>, JpaSpecificationExecutor<Note>{
}
