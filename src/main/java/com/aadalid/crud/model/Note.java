package com.aadalid.crud.model;


import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.validation.annotation.Validated;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Validated
@Entity
@Table(name="notes")
@Getter
@Setter
public class Note implements Serializable {
    private static final long serialVersionUID = 5057388942388599423L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 4000, nullable = false)
    private String title;

    @Column(length = 4000, nullable = false)
    private String content;

    @Column(nullable = false, updatable = false)
    @CreatedDate
    private LocalDateTime createdOn;

    @Column(nullable = false)
    @LastModifiedDate
    private LocalDateTime updatedOn;

    public String getShortContent() {
        return StringUtils.left(this.content, 200);
    }
}
