package com.aadalid.crud.service;

import com.aadalid.crud.model.Note;
import com.aadalid.crud.repository.INoteRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class NoteService {
    @Autowired
    private INoteRepository noteRepository;

    private boolean existsById(Long id) {
        return noteRepository.existsById(id);
    }

    public Note findById(Long id) {
        return noteRepository.findById(id).orElse(null);
    }

    public List<Note> findAll(int pageNumber, int rowPerPage) {
        List<Note> notes = new ArrayList<>();
        Pageable sortedByLastUpdateDesc = PageRequest.of(pageNumber - 1, rowPerPage, Sort.by("updatedOn").descending());

        noteRepository.findAll(sortedByLastUpdateDesc).forEach(notes::add);
        return notes;
    }

    public Note save(Note note) throws  Exception {
        if (StringUtils.isEmpty(note.getTitle())) {
            throw new Exception("title is requireD");
        }

        note.setCreatedOn(LocalDateTime.now());
        note.setUpdatedOn(LocalDateTime.now());

        return noteRepository.save(note);
    }

    public void update(Note note) throws Exception {
        if (StringUtils.isEmpty(note.getTitle())) {
            throw new Exception("title is requireD");
        }

        Optional<Note> note1 = noteRepository.findById(note.getId());
        note.setCreatedOn(note1.get().getCreatedOn());
        note.setUpdatedOn(LocalDateTime.now());

        noteRepository.save(note);
    }

    public void deleteById (Long id) throws Exception {
        if (!existsById(id)) {
            throw new Exception("cannot find note with id " + id);
        } else {
            noteRepository.deleteById(id);
        }
    }

    public Long count () {
        return noteRepository.count();
    }
}
