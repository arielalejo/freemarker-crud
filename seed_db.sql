 -- create user barista with password 'cappuccino';
 -- create database dariawan with template=template0 owner=barista;
 -- \connect dariawan;
 -- alter default privileges grant all on tables to barista;
 -- alter default privileges grant all on sequences to barista;

CREATE table notes (
    id bigint(20) NOT NULL AUTO_INCREMENT,
    title varchar(255) DEFAULT NULL,
    content varchar(255) DEFAULT NULL,
    created_on datetime NOT NULL,
    updated_on datetime NOT NULL,
    PRIMARY KEY (id)
) ENGINE=MYISAM DEFAULT CHARSET=latin1;

INSERT INTO notes (title, content, created_on, updated_on)
VALUES ('Hello World!', 'First note in notes application', '2019-11-07 08:00:00', '2019-11-07 18:30:45');


