## connect to mysql db 
## (solution to: access denied for user 'xxxx'@'172.17.0.1')
1.  start mysql docker container
 ```
docker run --name mysql-dev --rm -v ~/local/mysql-dev:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=admin -p 3306:3306 -p 33060:33060 -d mysql
```

 2. enter container
`docker exec -it mysql-dev bash`

 3. enter mysql db
 `mysql -u root -p`

    _password: admin_

 4. create db
    `create database dariawan;
    `

 5. create user;
`CREATE USER 'barista2'@'172.17.0.1' IDENTIFIED BY 'cappuccino';`

 6. grant privileges to user
```
GRANT ALL PRIVILEGES ON *.* TO 'barista2'@'172.17.0.1' WITH GRANT OPTION;
flush privileges;

```
 
 7. create table (notes)
```
use dariawan;
CREATE table notes (
    id bigint(20) NOT NULL AUTO_INCREMENT,
    title varchar(255) DEFAULT NULL,
    content varchar(255) DEFAULT NULL,
    created_on datetime NOT NULL,
    updated_on datetime NOT NULL,
    PRIMARY KEY (id)
    ) ENGINE=MYISAM DEFAULT CHARSET=latin1;

INSERT INTO notes (title, content, created_on, updated_on)
VALUES ('Hello World!', 'First note in notes application', '2019-11-07 08:00:00', '2019-11-07 18:30:45');
``` 
*or from a .sql file with `source FILE_NAME`.

8. you can start the sprint boot application